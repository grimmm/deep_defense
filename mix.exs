defmodule DeepDefense.Mixfile do
  use Mix.Project
  def project do
    [apps_path: "apps",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end
  defp deps do
    [{:accessible, "~> 0.2.0"}]
  end
end
