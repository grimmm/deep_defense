'''
usage: python gen_diff.py -h
'''

import argparse

from keras.models import load_model
from keras.datasets import mnist
from keras.layers import Input
from scipy.misc import imsave
from configs import bcolors
from io import StringIO
from utils import *
import boto3
import botocore
import numpy as np

# read the parameter
# argument parsing
parser = argparse.ArgumentParser(description='Main function for difference-inducing input generation in MNIST dataset')
parser.add_argument('model_path', help="The path on disk to the model you want to compute the coverage for. It must be in the h5 format and contain a keras Input layer as its entry point.")
parser.add_argument('dataset_id', help="The id of the dataset located on S3")
parser.add_argument('job_id', help="The id of the job that started this process or any unique identifier to reference this process.")
parser.add_argument('transformation', help="realistic transformation type", choices=['light', 'occl', 'blackout'])
parser.add_argument('weight_diff', help="weight hyperparm to control differential behavior", type=float)
parser.add_argument('weight_nc', help="weight hyperparm to control neuron coverage", type=float)
parser.add_argument('step', help="step size of gradient descent", type=float)
parser.add_argument('seeds', help="number of seeds of input", type=int)
parser.add_argument('grad_iterations', help="number of iterations of gradient descent", type=int)
parser.add_argument('threshold', help="threshold for determining neuron activated", type=float)
parser.add_argument('-t', '--target_model', help="target model that we want it predicts differently",
                    choices=[0, 1, 2], default=0, type=int)
parser.add_argument('-sp', '--start_point', help="occlusion upper left corner coordinate", default=(0, 0), type=tuple)
parser.add_argument('-occl_size', '--occlusion_size', help="occlusion size", default=(10, 10), type=tuple)

args = parser.parse_args()

BUCKET_NAME = 'deep.defense' # replace with your bucket name

s3 = boto3.resource('s3')

model_key  = f'models/{args.model_path}'
model_path = f'./{args.model_path}'

try:
    s3.Bucket(BUCKET_NAME).download_file(model_key, model_path)
except botocore.exceptions.ClientError as e:
    if e.response['Error']['Code'] == "404":
        exit("The model does not exist.")
    else:
        raise

dataset_key  = f'datasets/{args.dataset_id}'
dataset_path = f'./{args.dataset_id}'

try:
    s3.Bucket(BUCKET_NAME).download_file(dataset_key, dataset_path)
except botocore.exceptions.ClientError as e:
    if e.response['Error']['Code'] == "404":
        exit("The dataset does not exist.")
    else:
        raise

x_test = np.load(dataset_path)
# define input tensor as a placeholder
input_tensor = Input(shape=input_shape)

# grsab the stored model's input tensor
client_model = load_model(f'./{args.model_path}')
input_tensor = client_model.layers[0].input
# init coverage table
model_layer_dict = init_coverage_tables(client_model)

overall_average = 0

for _ in range(args.seeds):
    gen_img = np.expand_dims(random.choice(x_test), axis=0)
    orig_img = gen_img.copy()
    label = np.argmax(client_model.predict(gen_img)[0])

    orig_label = label
    layer_name1, index = neuron_to_cover(model_layer_dict)

    loss = -args.weight_diff * K.mean(client_model.layers[-2].output[..., orig_label])
    loss_neuron = K.mean(client_model.get_layer(layer_name1).output[..., index])
    layer_output = (loss) + args.weight_nc * (loss_neuron)

    final_loss = K.mean(layer_output)

    # we compute the gradient of the input picture wrt this loss
    grads = normalize(K.gradients(final_loss, input_tensor)[0])

    # this function returns the loss and grads given the input picture
    iterate = K.function([input_tensor], [loss, loss_neuron, grads])

    # we run gradient ascent for 20 steps
    for iters in range(args.grad_iterations):
        loss_value1, loss_neuron1, grads_value = iterate([gen_img])
        if args.transformation == 'light':
            grads_value = constraint_light(grads_value)  # constraint the gradients value
        elif args.transformation == 'occl':
            grads_value = constraint_occl(grads_value, args.start_point,
                                          args.occlusion_size)  # constraint the gradients value
        elif args.transformation == 'blackout':
            grads_value = constraint_black(grads_value)  # constraint the gradients value

        gen_img += grads_value * args.step
        predictions1 = np.argmax(client_model.predict(gen_img)[0])

        update_coverage(gen_img, client_model, model_layer_dict, args.threshold)

        print(bcolors.OKGREEN + 'covered percentage of %d neurons: %.3f'
              % (len(model_layer_dict), neuron_covered(model_layer_dict)[2]))

        averaged_nc = (neuron_covered(model_layer_dict)[0]) / float(neuron_covered(model_layer_dict)[1])
        overall_average += averaged_nc

        print(bcolors.OKGREEN + 'averaged covered neurons %.3f' % averaged_nc + bcolors.ENDC)

overall_average /= args.grad_iterations*args.seeds

s3.Bucket(BUCKET_NAME).put_object(Key=f'results/{args.job_id}', Body=f'The overall network coverage from all runs was: {overall_average}'.encode("utf-8"))
