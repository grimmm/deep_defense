defprotocol JobProtocols do
  @doc """
    spawn_job is the part of the protocol that must define how the job in question is created and executed.
  """
  def spawn_job(job)

  @doc """
    spawn_job is the part of the protocol that must define how the job in question is created and executed. This version includes an argument for aws credentials.
  """
  def spawn_job(job, aws_creds)

  @doc """
    get_job_status returns the k8 status for a previously executed job.
  """
  def get_job_status(job_id)

  @doc """
    define_job returns the k8 job definition to be run.
  """
  def define_job(job, config_object \\ {})
end
