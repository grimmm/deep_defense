defmodule DefenseServices.XploreMonitor do
  require Kazan
  alias DefenseServices.XploreMonitor

  defstruct type: :xplore, job_id: nil, model_id: nil, dataset_id: nil, start_time: nil, end_time: nil, status: "DOWN", errors: []

  use Accessible

  defimpl JobProtocols do
    def get_job_status(job = %XploreMonitor{}) do
      {status, spec} = Kazan.Apis.Batch.V1.read_namespaced_job_status("default", "xplore-#{job[:job_id]}", [{:pretty, true}])

      IO.inspect spec

      {exec_status, run_results} = case status do
        :ok -> Kazan.run(spec)
        _   -> {:error, spec}
      end

      case exec_status do
        :ok ->
          %{ status: results } = run_results
          job = cond do
            results.succeeded != nil -> Map.put(job, :status, "SUCCESS")
            results.failed != nil -> Map.put(job, :status, "FAILED")
            results.active != nil -> Map.put(job, :status, "ACTIVE")
          end
          IO.inspect job
          Map.put(job, :start_time, results.start_time)
        :error ->
          job_res = Map.put(job, :errors, job[:errors] ++ ["Unable to get job status please try again later."])
          Map.put(job_res, :status, "ERROR")
      end
    end

    def spawn_job(job = %XploreMonitor{}, aws_creds) do
      job_spec = define_job(job, aws_creds)

      {status, spec} = Kazan.Apis.Batch.V1.create_namespaced_job(job_spec, "default", [{"pretty", true}])

      {exec_status, _} = case status do
        :ok -> Kazan.run(spec)
        _   -> {:error, spec}
      end

      job = case exec_status do
        :ok ->
          Map.put(job, :start_time, DateTime.to_iso8601(DateTime.utc_now()))
        :error ->
          Map.put(job, :errors, job[:errors] ++ ["Unable to start job please try again later."])
      end

      job
    end

    def define_job(job, aws_creds) do
      %Kazan.Apis.Batch.V1.Job{
        api_version: nil,
        kind:        nil,
        metadata:    define_metadata(job[:job_id]),
        spec:        %Kazan.Apis.Batch.V1.JobSpec{
          template: %Kazan.Apis.Core.V1.PodTemplateSpec{
            spec: %Kazan.Apis.Core.V1.PodSpec{
              containers: [define_container(job[:model_id], job[:dataset_id], job[:job_id], aws_creds)],
              restart_policy: "OnFailure"
            }
          }
        }
      }
    end

    defp define_container(model_id, dataset_id, job_id, aws_creds) do
      IO.inspect aws_creds
      %Kazan.Apis.Core.V1.Container{
        image: Application.get_env(:defense_services, :xplore_container),
        # ideally will move the below job config into a config map of somekind to be tweaked at the calling level
        command: ["python", "./gen_diff.py", model_id, dataset_id, job_id, "light", "0.01", "0.1", "10", "12", "10", "0.1"],
        name: job_id,
        env: [
          %Kazan.Apis.Core.V1.EnvVar{ name: "AWS_ACCESS_KEY_ID", value: aws_creds[:aws_access_key_id] },
          %Kazan.Apis.Core.V1.EnvVar{ name: "AWS_SECRET_ACCESS_KEY", value: aws_creds[:aws_secret_access_key]},
          %Kazan.Apis.Core.V1.EnvVar{ name: "AWS_DEFAULT_REGION", value: "us-east-1" }
        ]
      }
    end

    defp define_metadata(job_id) do
      %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
        name: "xplore-#{job_id}"
      }
    end
  end

end
