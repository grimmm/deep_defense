defmodule ApiWeb.FinePruningControllerTest do
  use ApiWeb.ConnCase

  alias Api.Defenses
  alias Api.Defenses.FinePruning

  @create_attrs %{}
  @update_attrs %{}
  @invalid_attrs %{}

  def fixture(:fine_pruning) do
    {:ok, fine_pruning} = Defenses.create_fine_pruning(@create_attrs)
    fine_pruning
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all fine_prunings", %{conn: conn} do
      conn = get conn, fine_pruning_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create fine_pruning" do
    test "renders fine_pruning when data is valid", %{conn: conn} do
      conn = post conn, fine_pruning_path(conn, :create), fine_pruning: @create_attrs
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get conn, fine_pruning_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id}
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, fine_pruning_path(conn, :create), fine_pruning: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update fine_pruning" do
    setup [:create_fine_pruning]

    test "renders fine_pruning when data is valid", %{conn: conn, fine_pruning: %FinePruning{id: id} = fine_pruning} do
      conn = put conn, fine_pruning_path(conn, :update, fine_pruning), fine_pruning: @update_attrs
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get conn, fine_pruning_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id}
    end

    test "renders errors when data is invalid", %{conn: conn, fine_pruning: fine_pruning} do
      conn = put conn, fine_pruning_path(conn, :update, fine_pruning), fine_pruning: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete fine_pruning" do
    setup [:create_fine_pruning]

    test "deletes chosen fine_pruning", %{conn: conn, fine_pruning: fine_pruning} do
      conn = delete conn, fine_pruning_path(conn, :delete, fine_pruning)
      assert response(conn, 204)
      assert_error_sent 404, fn ->
        get conn, fine_pruning_path(conn, :show, fine_pruning)
      end
    end
  end

  defp create_fine_pruning(_) do
    fine_pruning = fixture(:fine_pruning)
    {:ok, fine_pruning: fine_pruning}
  end
end
