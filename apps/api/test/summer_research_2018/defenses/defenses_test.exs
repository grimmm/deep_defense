defmodule Api.DefensesTest do
  use Api.DataCase

  alias Api.Defenses

  describe "fine_prunings" do
    alias Api.Defenses.FinePruning

    @valid_attrs %{}
    @update_attrs %{}
    @invalid_attrs %{}

    def fine_pruning_fixture(attrs \\ %{}) do
      {:ok, fine_pruning} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Defenses.create_fine_pruning()

      fine_pruning
    end

    test "list_fine_prunings/0 returns all fine_prunings" do
      fine_pruning = fine_pruning_fixture()
      assert Defenses.list_fine_prunings() == [fine_pruning]
    end

    test "get_fine_pruning!/1 returns the fine_pruning with given id" do
      fine_pruning = fine_pruning_fixture()
      assert Defenses.get_fine_pruning!(fine_pruning.id) == fine_pruning
    end

    test "create_fine_pruning/1 with valid data creates a fine_pruning" do
      assert {:ok, %FinePruning{} = fine_pruning} = Defenses.create_fine_pruning(@valid_attrs)
    end

    test "create_fine_pruning/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Defenses.create_fine_pruning(@invalid_attrs)
    end

    test "update_fine_pruning/2 with valid data updates the fine_pruning" do
      fine_pruning = fine_pruning_fixture()
      assert {:ok, fine_pruning} = Defenses.update_fine_pruning(fine_pruning, @update_attrs)
      assert %FinePruning{} = fine_pruning
    end

    test "update_fine_pruning/2 with invalid data returns error changeset" do
      fine_pruning = fine_pruning_fixture()
      assert {:error, %Ecto.Changeset{}} = Defenses.update_fine_pruning(fine_pruning, @invalid_attrs)
      assert fine_pruning == Defenses.get_fine_pruning!(fine_pruning.id)
    end

    test "delete_fine_pruning/1 deletes the fine_pruning" do
      fine_pruning = fine_pruning_fixture()
      assert {:ok, %FinePruning{}} = Defenses.delete_fine_pruning(fine_pruning)
      assert_raise Ecto.NoResultsError, fn -> Defenses.get_fine_pruning!(fine_pruning.id) end
    end

    test "change_fine_pruning/1 returns a fine_pruning changeset" do
      fine_pruning = fine_pruning_fixture()
      assert %Ecto.Changeset{} = Defenses.change_fine_pruning(fine_pruning)
    end
  end
end
