defmodule Api.Defenses do
  @moduledoc """
  The Defenses context.
  """

  require IEx

  alias ExAws.S3

  alias Api.Defenses.FinePruning
  alias Api.Defenses.DeepXplore

  def start_job(:xplore, model_id, dataset_id) do
    aws_creds = %{
      "aws_access_key_id": Application.get_env(:api, :aws)[:access_key_id],
      "aws_secret_access_key": Application.get_env(:api, :aws)[:secret_access_key]
    }

    DeepXplore.start_job(model_id, dataset_id, aws_creds)
  end

  def get_job_status(:xplore, job_id) do
    DeepXplore.get_job_status(job_id)
  end

  def get_job_results(job_id) do
    {status, res} = S3.get_object(Application.get_env(:api, :result_bucket), job_id)
                    |> ExAws.request
    case status do
      :ok ->
        %{body: body, headers: _, status_code: code} = res
        case code do
          200 -> %{ status: 200, response: body }
          404 -> %{ status: code, respoinse: "Could not find results for job, check to see if your job is still running." }
          _   -> %{ status: code, response: %{error: body} }
        end
      :error ->
        {_, _, error_res} = res
        %{status_code: status, body: body} = error_res
        %{status: status, response: %{error: body}}
    end
  end

  @doc """
      Upload base64'd model to an s3 bucket.
  """

  def upload_to_s3(name, upload, bucket_name) do
    upload.path
    |> File.read
    |> case do
      :error -> %{status: 422, response: %{error: "Invalid File"}}
      {:ok, model} -> make_s3_upload_request(model, name, bucket_name)
    end
  end

  defp make_s3_upload_request(contents, name, bucket) do
    {type, res} = S3.put_object(bucket, name, contents)
                  |> ExAws.request
    case type do
      :ok ->
        %{status_code: status, body: body} = res
        case status do
          200 -> %{status: 200, response: %{name: name, body: body}}
          _   -> %{status: status, response: %{error: body}}
        end
      :error ->
        {_, _, error_res} = res
        %{status_code: status, body: body} = error_res
        %{status: status, response: %{error: body}}
    end
  end
end
