defmodule ApiWeb.ModelView do
  use ApiWeb, :view
  alias ApiWeb.ModelView
  require IEx

  def render("upload.json", %{status: status, uuid: uuid}) do
    %{status: status, uuid: uuid}
  end

  def render("error.json", %{error_response: res}) do
    res
  end
end
