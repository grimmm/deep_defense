defmodule ApiWeb.JobView do
  use ApiWeb, :view
  alias ApiWeb.JobView
  require IEx

  def render("create_job.json", %{job: job}) do
    job
  end

  def render("result.json", %{result: result}) do
    result
  end

  def render("status.json", %{job: job}) do
    job
  end

  def render("error.json", %{error_response: res}) do
    res
  end
end
