defmodule ApiWeb.ModelController do
  use ApiWeb, :controller

  require IEx
  alias Api.Defenses
  require UUID

  def create(conn, %{"model" => model}) do
    model_uuid          = UUID.uuid4()
    model_bucket_name   = Application.get_env(:api, :model_bucket)
    %{ status: status } = res = Defenses.upload_to_s3(model_uuid, model, model_bucket_name)

    case status do
      200 -> render(conn, "upload.json", status: status, uuid: model_uuid)
      _ ->
        conn
        |> put_status(res[:status])
        |> render("error.json", error_response: res)
    end
  end

  def create_model_dataset(conn, %{"dataset" => dataset}) do
    dataset_uuid        = UUID.uuid4()
    dataset_bucket_name = Application.get_env(:api, :dataset_bucket)
    %{ status: status } = res = Defenses.upload_to_s3(dataset_uuid, dataset, dataset_bucket_name)

    case status do
      200 -> render(conn, "upload.json", status: status, uuid: dataset_uuid)
      _ ->
        conn
        |> put_status(res[:status])
        |> render("error.json", error_response: res)
    end
  end
end
